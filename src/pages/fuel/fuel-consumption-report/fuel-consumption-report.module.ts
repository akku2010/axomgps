import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelConsumptionReportPage } from './fuel-consumption-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { OnCreate2Module } from './dummy2-directive.module';

@NgModule({
  declarations: [
    FuelConsumptionReportPage
  ],
  imports: [
    IonicPageModule.forChild(FuelConsumptionReportPage),
    SelectSearchableModule,
    OnCreate2Module
  ]
})
export class FuelConsumptionReportPageModule {}
