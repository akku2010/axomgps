import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IgnitionReportPage } from './ignition-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    IgnitionReportPage,
  ],
  imports: [
    IonicPageModule.forChild(IgnitionReportPage),
    SelectSearchableModule
  ],
})
export class IgnitionReportPageModule {}
