import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcReportPage } from './ac-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    AcReportPage,
  ],
  imports: [
    IonicPageModule.forChild(AcReportPage),
    SelectSearchableModule
  ],
})
export class AcReportPageModule {}
