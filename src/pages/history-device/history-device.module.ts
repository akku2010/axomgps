import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryDevicePage } from './history-device';
import { IonBottomDrawerModule } from '../../../node_modules/ion-bottom-drawer';
import { SelectSearchableModule } from 'ionic-select-searchable';
// import { ModalPage } from './modal';

@NgModule({
  declarations: [
    HistoryDevicePage
  ],
  imports: [
    IonicPageModule.forChild(HistoryDevicePage),
    IonBottomDrawerModule,
    SelectSearchableModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  // entryComponents: [ModalPage]
  // providers: [DatePicker]
})
export class HistoryDevicePageModule { }
