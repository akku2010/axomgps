import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { POIReportPage } from './poi-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    POIReportPage
  ],
  imports: [
    IonicPageModule.forChild(POIReportPage),
    SelectSearchableModule
  ],
})
export class POIReportPageModule {}
